<h1>A program to find and download the latest svtplay-dl (Search-svtplay-dl)</h1>
<p>Read more and download at the <a href="https://gitlab.com/posktomten/checkversionsvtplaydl/-/wikis/home">wiki pages.</a></p>

The program uses these libraries:<br>
- [libabout](https://gitlab.com/posktomten/libabout)<br>
- [libcreateshortcut, Linux](https://gitlab.com/posktomten/libcreateshortcut)<br>
- [libcreateshortcut, Windows](https://gitlab.com/posktomten/libcreateshortcut_windows)<br>
- [download_install](https://gitlab.com/posktomten/download_offline_installer) (Windos)<br>
- [libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage) (Linux)<br>
