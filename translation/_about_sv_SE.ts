<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>About</name>
    <message>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <source>Copyright © </source>
        <translation>Copyright © </translation>
    </message>
    <message>
        <source>License: </source>
        <translation>Licens: </translation>
    </message>
    <message>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>distribueras i hopp om att den ska vara användbar, men UTAN NÅGON GARANTI; utan ens underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR ETT SÄRSKILT SYFTE.</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Webbsida</translation>
    </message>
    <message>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <source>Compiled on: </source>
        <translation>Kompilerad på: </translation>
    </message>
    <message>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation>Se GNU General Public License version 3.0 för mer information.</translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation>Körs på: </translation>
    </message>
    <message>
        <source> is in the folder: </source>
        <translation> finns i mappen: </translation>
    </message>
    <message>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation>Kompilator: LLVM/Clang/LLD baserad MinGW version </translation>
    </message>
    <message>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Kompilator: MinGW (GCC för Windows) version </translation>
    </message>
    <message>
        <source>Programming language: C++</source>
        <translation>Programmeringsspråk: C++</translation>
    </message>
    <message>
        <source>C++ version: C++20</source>
        <translation>C++ version: C++20</translation>
    </message>
    <message>
        <source>C++ version: C++17</source>
        <translation>C++ version: C++17</translation>
    </message>
    <message>
        <source>C++ version: C++14</source>
        <translation>C++ version: C++14</translation>
    </message>
    <message>
        <source>C++ version: C++11</source>
        <translation>C++ version: C++11</translation>
    </message>
    <message>
        <source>C++ version: Unknown</source>
        <translation>C++ version: Okänd</translation>
    </message>
    <message>
        <source>Application framework: Qt version </source>
        <translation>Applikationsramverk: Qt version </translation>
    </message>
    <message>
        <source>Processor architecture: 32-bit</source>
        <translation>Processorarkitektur: 32-bitar</translation>
    </message>
    <message>
        <source>Processor architecture: 64-bit</source>
        <translation>Processorarkitektur: 64-bitar</translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation>Kompilator: Clang version </translation>
    </message>
</context>
</TS>
