#!/bin/bash

QT5="5.15.16"
QT6="6.8.2"
EXECUTABLE="checkversionsvtplaydl/"

if [[ -x "/opt/Qt/${QT6}/gcc_64/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/${QT6}/gcc_64/bin/"

    echo "${QT6}"
    
elif [[ -x "/opt/Qt/${QT5}/gcc_64/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/${QT5}/gcc_64/bin/"

    echo "${QT5}"

else

	echo "Cannot find executable program files."
	exit 0
	
fi

date_now=$(date "+%FT%H-%M-%S")
mkdir -p all_translations-${date_now}/ts



FILES=($(ls *.ts))
#echo ${FILES[*]}

for value in "${FILES[@]}"

do
   if [[ "${value}" != "_complete_"* ]] ; then

  cp $value all_translations-${date_now}/ts

  fi

done

tar -czf all_translations-${date_now}.tar.gz all_translations-${date_now}


fil=all_translations-${date_now}.tar.gz

HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/${EXECUTABLE}/translations"
put $fil
bye
EOF

/usr/bin/zip -r all_translations-${date_now}.zip all_translations-${date_now}
fil=all_translations-${date_now}.zip


ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/${EXECUTABLE}/translations"
put $fil
bye
EOF

rm all_translations-${date_now}.tar.gz
rm all_translations-${date_now}.zip
