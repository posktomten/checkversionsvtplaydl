<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.cpp" line="80"/>
        <source>Checking if there is a new version...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="86"/>
        <location filename="../dialog.cpp" line="355"/>
        <source>Please download the new &quot;portable&quot; version.&lt;br&gt;&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="83"/>
        <location filename="../dialog.cpp" line="89"/>
        <location filename="../dialog.cpp" line="352"/>
        <location filename="../dialog.cpp" line="358"/>
        <source>Please &quot;Right click&quot;, &quot;Update Search-svtplay-dl...&quot; to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="108"/>
        <source>Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="115"/>
        <source>Search for latest svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="118"/>
        <source>SSL cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="119"/>
        <source>Your operating system does not support SSL.
Unfortunately, it is not possible to contact svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="126"/>
        <source>Download the latest official Linux version of svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="131"/>
        <source>Download the latest official Windows 32-bit version of svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="136"/>
        <source>Download the latest official Windows 64-bit version of svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="144"/>
        <source>Check Search-svtplay-dl for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="147"/>
        <source>Update Search-svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="150"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="152"/>
        <source>Create desktop shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="154"/>
        <source>Delete desktop shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="157"/>
        <source>Delete old version and unzip the new one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>The new version of svtplay-dl will be unpacked.&lt;br&gt;If there is an older version in the &quot;svtplay-dl&quot; folder,&lt;br&gt;the old folder will be deleted&lt;br&gt;before the new version is unpacked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="166"/>
        <source>Create applications menu shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="168"/>
        <source>Delete applications menu shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="172"/>
        <source>Remove all settings and exit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="175"/>
        <source>Uninstall...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="178"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="180"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="183"/>
        <source>Light Fusion Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="185"/>
        <source>Dark Fusion Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="189"/>
        <source>MS Windows Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="409"/>
        <source>This program&apos;s task is to search for and download the latest svtplay-dl from &lt;br&gt;&lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="411"/>
        <source>Search-svtplay-dl uses &lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://www.7-zip.org/&quot;&gt;7-Zip&lt;/a&gt; to unpack the compressed zip files with svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="463"/>
        <source>The uninstaller cannot be found, or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="522"/>
        <source>Search for the latest version of svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="523"/>
        <source>svtplay-dl web server could not be found. No data can be received.
Check your network and check if the svtplay-dl web server is running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="572"/>
        <source>The official version and your version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="579"/>
        <source>Official version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="581"/>
        <location filename="../dialog.cpp" line="601"/>
        <source>Your version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="599"/>
        <source>The official version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="613"/>
        <source>The latest version of svtplay-dl is from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="614"/>
        <source>You have not specified which version of svtplay-dl you have. If you have the latest version,&lt;br&gt;click &quot;Save&quot;.&lt;br&gt;Or download the latest version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="639"/>
        <source>is saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="642"/>
        <source>The date and time is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="59"/>
        <source>Downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="74"/>
        <source>Select download folder for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="75"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="76"/>
        <location filename="../language.cpp" line="49"/>
        <location filename="../language.cpp" line="109"/>
        <location filename="../style.cpp" line="51"/>
        <location filename="../style.cpp" line="105"/>
        <location filename="../style.cpp" line="161"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <source>Error opening file for write: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="106"/>
        <source>Error write to file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="116"/>
        <source>&lt;br&gt;Click &quot;Save&quot; to save the version information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="169"/>
        <source>The unpacking is complete.&lt;br&gt;Click &quot;Save&quot; to save the version information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="171"/>
        <location filename="../download.cpp" line="194"/>
        <source>Unpacking error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="189"/>
        <source>Starting to unpack...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="201"/>
        <source>Deleting file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="215"/>
        <source>Please check the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="230"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="161"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="192"/>
        <source>Save the date and time of the your latest version of svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="195"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="220"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="262"/>
        <source>Linux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="287"/>
        <source>Windows 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="312"/>
        <source>Windows 64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="45"/>
        <location filename="../language.cpp" line="105"/>
        <location filename="../style.cpp" line="47"/>
        <location filename="../style.cpp" line="101"/>
        <location filename="../style.cpp" line="157"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="47"/>
        <location filename="../language.cpp" line="107"/>
        <location filename="../style.cpp" line="49"/>
        <location filename="../style.cpp" line="103"/>
        <location filename="../style.cpp" line="159"/>
        <source>On next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="56"/>
        <source>The program needs to be restarted to switch to Swedish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="116"/>
        <source>The program needs to be restarted to switch to English.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="58"/>
        <source>The program needs to be restarted to switch to Fusion light theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="112"/>
        <source>The program needs to be restarted to switch to Fusion dark theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="168"/>
        <source>The program needs to be restarted to switch to MS Windows theme.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright © </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiled on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is in the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Programming language: C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application framework: Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 32-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 64-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadInstall</name>
    <message>
        <location filename="../download_install.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="54"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="70"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="86"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="41"/>
        <source>Download </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="60"/>
        <location filename="../download_install.cpp" line="72"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="90"/>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="28"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="32"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="36"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="40"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="44"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="48"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="52"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="56"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="60"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="64"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="68"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="72"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="76"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="80"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="84"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="88"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="92"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="96"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="100"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="104"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="108"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="112"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="116"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="120"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="124"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="128"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="132"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="136"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="140"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="144"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="148"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="152"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="156"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="160"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="164"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="39"/>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="71"/>
        <location filename="../checkupdate.cpp" line="97"/>
        <location filename="../checkupdate.cpp" line="116"/>
        <location filename="../checkupdate.cpp" line="126"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="173"/>
        <location filename="../checkupdate.cpp" line="210"/>
        <location filename="../checkupdate.cpp" line="286"/>
        <location filename="../checkupdate.cpp" line="297"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="96"/>
        <location filename="../checkupdate.cpp" line="176"/>
        <location filename="../checkupdate.cpp" line="296"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="114"/>
        <source>Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="115"/>
        <source> is newer than the latest official version. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="125"/>
        <source>You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="136"/>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="209"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="256"/>
        <source>Updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="282"/>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="284"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="49"/>
        <location filename="../deleteallsettings.cpp" line="92"/>
        <location filename="../deleteallsettings.cpp" line="105"/>
        <location filename="../deleteallsettings.cpp" line="119"/>
        <location filename="../deleteallsettings.cpp" line="141"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="47"/>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="109"/>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="26"/>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="40"/>
        <source>All your saved settings will be deleted.
All shortcuts will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="29"/>
        <source>&lt;b&gt;Warning! 2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="34"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="35"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="42"/>
        <source>All your saved settings will be deleted.
Desktop shortcut will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="43"/>
        <source>KUKEN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="90"/>
        <location filename="../deleteallsettings.cpp" line="139"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="104"/>
        <location filename="../deleteallsettings.cpp" line="118"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>The shortcut could not be removed:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="42"/>
        <location filename="../update.cpp" line="75"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="43"/>
        <source>zsync cannot be found in path:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="44"/>
        <source>Unable to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="76"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="84"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../update.cpp" line="89"/>
        <source>is updated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../updatedialog.ui" line="35"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="45"/>
        <source>Updating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="83"/>
        <source>Updating, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
