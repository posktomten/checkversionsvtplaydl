<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="49"/>
        <location filename="../deleteallsettings.cpp" line="92"/>
        <location filename="../deleteallsettings.cpp" line="105"/>
        <location filename="../deleteallsettings.cpp" line="119"/>
        <location filename="../deleteallsettings.cpp" line="141"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>The shortcut could not be removed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="29"/>
        <source>&lt;b&gt;Warning! 2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="34"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="35"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="40"/>
        <source>All your saved settings will be deleted.
All shortcuts will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="42"/>
        <source>All your saved settings will be deleted.
Desktop shortcut will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="43"/>
        <source>KUKEN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="90"/>
        <location filename="../deleteallsettings.cpp" line="139"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="104"/>
        <location filename="../deleteallsettings.cpp" line="118"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
