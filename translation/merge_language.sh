#!/bin/bash

QT5="5.15.16"
QT6="6.8.2"

HERE="$(pwd)"
#LUPDATE_OPTIONS="-no-ui-lines -no-obsolete -locations none -disable-heuristic sametext -disable-heuristic similartext"
echo ${HERE}

# Find qmake, to also find lupdate, lconvert and lrelease

if [[ -x "/opt/Qt/${QT6}/gcc_64/bin/qmake" ]]

then

  SOKVAG="/opt/Qt/${QT6}/gcc_64/bin/"

elif [[ -x "/opt/Qt/${QT5}/bin/qmake" ]]


then

    SOKVAG="/opt/Qt/${QT5}/gcc_64/bin/"

else

	echo "Cannot find executable program files."
	exit 1

fi




cp -f ../code/i18n/_checkversionsvtplaydl_sv_SE.ts ./_checkversionsvtplaydl_sv_SE.ts
cp -f ../code/i18n/_checkversionsvtplaydl_xx_XX.ts ./_checkversionsvtplaydl_xx_XX.ts



${SOKVAG}lconvert _checkversionsvtplaydl_sv_SE.ts _about_sv_SE.ts _download_install_sv_SE.ts _libcheckupdate_sv_SE.ts _libcreateshortcut_lin_sv_SE.ts _libcreateshortcut_win_sv_SE.ts _libupdateappimage_sv_SE.ts -o _complete_sv_SE.ts

${SOKVAG}lconvert _checkversionsvtplaydl_xx_XX.ts _about_xx_XX.ts _download_install_xx_XX.ts _libcheckupdate_xx_XX.ts _libcreateshortcut_lin_xx_XX.ts _libcreateshortcut_win_xx_XX.ts _libupdateappimage_xx_XX.ts -o _complete_xx_XX.ts



${SOKVAG}lrelease _complete_sv_SE.ts

cp -f _complete_sv_SE.qm ../code/i18n/_complete_sv_SE.qm




