<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="42"/>
        <location filename="../update.cpp" line="75"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="43"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="44"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="76"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="89"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="84"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.ui" line="35"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="45"/>
        <source>Updating...</source>
        <translation>Uppdaterar...</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="83"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
</TS>
