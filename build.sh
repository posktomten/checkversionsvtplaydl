#!/bin/bash


      QT=6.8.2
      EXECUTABLE=checkversionsvtplaydl
      GLIBC=$(glibcversion);

        HERE=$(pwd)


        cd ${EXECUTABLE}-zsyncmake
        rm *"${EXECUTABLE}"*
        rm ".invisible_"*


      cd ..
      rm *"${EXECUTABLE}-x86_64.AppImage"*
      rm ".invisible_"*





      cd code

        export CXX=/bin/g++
        export CMAKE_CXX_OUTPUT_EXTENSION=.o
        export CMAKE_C_COMPILER=/bin/gcc
        export CMAKE_PREFIX_PATH=/opt/Qt/${QT}/gcc_64
        export QT_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6
        export QT_QMAKE_EXECUTABLE=/opt/Qt/${QT}//gcc_64/bin/qmake
        export Qt6CoreTools_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6CoreTools
        export Qt6Core_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6Core
        export Qt6LinguistTools_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6LinguistTools
        export Qt6Network_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6Network
        export Qt6_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6
        export CMAKE_BUILD_TYPE=Release
        export CMAKE_GENERATOR=Ninja
        export CMAKE_PREFIX_PATH=/opt/Qt/${QT}/gcc_64
        export QT_DIR=/opt/Qt/${QT}/gcc_64
        export Qt6_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6
        export CMAKE_MODULE_PATH=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6





        cmake -S "." -B "../build" -G "Ninja" -DCMAKE_BUILD_TYPE=Release
        cmake --build "../build" --target all

        cd ..
        cp build-executable6/* linuxdeploy/
        rm -R build
        rm -R build-executable6

        cd linuxdeploy

       ./instructions.sh

echo "Finished!"
