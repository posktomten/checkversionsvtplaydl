#  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#          checkversionsvtplaydl
#          Copyright (C) 2020 - 2023 Ingemar Ceicer
#          https://gitlab.com/posktomten/checkversionsvtplaydl
#          programming@ceicer.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT       += core gui network widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11



# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



TARGET = checkversionsvtplaydl

SOURCES += \
    deletesettings.cpp \
    main.cpp \
    dialog.cpp

HEADERS += \
    dialog.h

FORMS += \
    dialog.ui

win32:RC_ICONS += resources/checkversionsvtplaydl.ico

#compiler=$$basename(QMAKESPEC)
#message(compiler: $$compiler)
#win32-msvc
#win32-g++

unix:INCLUDEPATH += "../include_lin"
unix:DEPENDPATH += "../include_lin"

win32:INCLUDEPATH += "../include_win"
win32:DEPENDPATH += "../include_win"

DESTDIR=../build-executable5

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

equals(QT_MAJOR_VERSION, 5) {
contains(QT_ARCH, x86_64) {


#unix:LIBS += -L../lib5/ -lssl
#unix:LIBS += -L../lib5/ -lcrypto

unix:CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug
unix:CONFIG (release, debug|release): LIBS += -L../lib5/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug

win32-g++:CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug
win32-g++:CONFIG (release, debug|release): LIBS += -L../lib5/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug

win32-msvc:CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -lcreateshortcutd # Debug
win32-msvc:CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -laboutd # Debug
}
}
contains(QT_ARCH, i386) {


# LIBS += -L../lib5_32/ -lssl
# LIBS += -L../lib5_32/ -lcrypto

CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcreateshortcutd # Debug
CONFIG (release, debug|release): LIBS += -L../lib5_32/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -laboutd # Debug

}
equals(QT_MAJOR_VERSION, 6) {
DESTDIR=../build-executable6
CONFIG (release, debug|release): LIBS += -L../lib6/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcreateshortcutd # Debug
CONFIG (release, debug|release): LIBS += -L../lib6/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -laboutd # Debug
}
#unix:UI_DIR = ../code
#win32:UI_DIR = ../code

RESOURCES += \
    resources.qrc

TRANSLATIONS += \
    i18n/checkversionsvtplaydl_sv_SE.ts
