//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef DIALOG_H
#define DIALOG_H
#include <QDialog>
#include <QNetworkReply>

#ifdef Q_OS_LINUX     // Linux
#include "updatedialog.h"
#define FONT 10
#endif
#ifdef Q_OS_WINDOWS     // Windows
#define FONT 10
#define PORTABLE
#endif

// #define LATEST_SVTPLAY_DL "2020-06-02T15:41:30"
#define URL_SNAPSHOTS "https://svtplay-dl.se/download/snapshots/"
#define URL_DOWNLOAD "https://svtplay-dl.se/archive/"
#define URL_OPTIONS "https://svtplay-dl.se/options/"
#define URL_SUPORTED_SITES "https://svtplay-dl.se/sites/"
#define URL_GITHUB "https://github.com/spaam/svtplay-dl"
#define URL_ISSUES "https://github.com/spaam/svtplay-dl/issues"
#define DISPLAY_NAME "Search-svtplay-dl"
#define EXECUTABLE_NAME "checkversionsvtplaydl"
#define VERSION "2.3.8"
#define DISPLAY_VERSION VERSION
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define SOURCECODE "https://gitlab.com/posktomten/checkversionsvtplaydl"
#define WEBSITE "https://gitlab.com/posktomten/checkversionsvtplaydl/-/wikis/home"
#define CHANGELOG "https://gitlab.com/posktomten/checkversionsvtplaydl/-/blob/master/CHANGELOG"
#define LICENSE "GNU General Public License v3.0"
#define LICENSE_LINK "https://gitlab.com/posktomten/checkversionsvtplaydl/-/blob/master/LICENSE"
#define COPYRIGHT "Ingemar Ceicer"
#define EMAIL "programming@ceicer.com"
#define COPYRIGHT_YEAR "2020"
#define CATEGORIES "AudioVideo"

#if defined(Q_OS_LINUX)
#define DOWNLOAD_SERVER "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/"
#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_linux.txt"
#endif // defined(Q_OS_LINUX)



//**//
#if defined(Q_OS_LINUX)

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Ubuntu 24.04.1 LTS 64-bit, GLIBC 2.39"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.39/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.39/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif  // BETA
#endif // __GLIBC_MINOR__ == 39

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.5 LTS 64-bit, GLIBC 2.35"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.35/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif  // BETA
#endif // __GLIBC_MINOR__ == 35

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.31/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/template/bin/linux/GLIBC2.31/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif  // BETA
#endif // __GLIBC_MINOR__ == 31

#endif // Linux

//**//

#if defined(Q_OS_WINDOWS)
#define COMPILEDON "Windows 11 Pro 24H2<br>OS build: 26100.2894"
#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows.txt"
#if !defined(PORTABLE)
#define PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/windows/"
// #define PATH "https://ceicer.eu/" EXECUTABLE_NAME "/bin/windows/"
#define FILENAME EXECUTABLE_NAME "_64-bit_setup.exe"
#endif
#endif //Q_OS

QT_BEGIN_NAMESPACE
namespace Ui
{
class Dialog;
}

QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();
    void setEndConfig();

private:
    Ui::Dialog *ui;
    // bool deleteSettings{};
    QString downloadpath;
    void download(QString filename);
    QString latestversion;
    QString networkErrorMessages(QNetworkReply::NetworkError &error);
    void messageBoxCritical(const QString & text);
    // void deleteAllSettings();
#if defined(Q_OS_LINUX)
    UpdateDialog *ud;
#endif
#if defined(Q_OS_WINDOWS)
    void extract(QString toExtract);
    QAction *actionExtract;
#endif
    void swedish();
    void english();
    void light();
    void dark();
    qint64 totalfiles;
    QString theme;


public slots:

    // download
    void downloadProgress(qint64 a, qint64 b);
    void getFileForSave(QByteArray filearray, QString filename);
    void error(QNetworkReply::NetworkError error);
#if defined(Q_OS_WINDOWS)
    // delete
    void inprogress(QString filename, qint64 todelete);
    void filesTodelete(qint64 antal);

    // extract
    void extractingStarted();
    void extractError(QString error);
#endif
};

#endif // DIALOG_H
