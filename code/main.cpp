//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include <QTranslator>
#include <QApplication>
#include <QFontDatabase>
#include <QSettings>
#include <QDirIterator>
#include <QMessageBox>
#include <QCoreApplication>
#include <QApplication>
#if defined(Q_OS_LINUX)
#include <QProcess>

#endif
#include <Qt>
#include <QStyleHints>
#include <QStyleFactory>
int main(int argc, char *argv[])
{
#ifdef _WIN32
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

    if(AttachConsole(ATTACH_PARENT_PROCESS)) {
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
    }

#endif
#endif
    QGuiApplication::sync();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif
    // QApplication *application = new QApplication(argc, argv);
    QApplication application(argc, argv);
#if defined(Q_OS_LINUX)
    QString fontPath = QStringLiteral(":/resources/Ubuntu-R.ttf");
#endif
#if defined(Q_OS_WINDOWS)
    QString fontPath = QStringLiteral(":/resources/segoeui.ttf");
#endif
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
#if defined(Q_OS_LINUX)
        QFont f(QStringLiteral("Ubuntu"), FONT);
#endif
#if defined(Q_OS_WINDOWS)
        QFont f(QStringLiteral("Segou UI"), FONT);
#endif
        QApplication::setFont(f);
    }

    /** LANGUAGE **/
#if defined(Q_OS_WINDOWS)
    QDir dir(QCoreApplication::applicationDirPath());
#elif defined(Q_OS_LINUX)
    QString appImagePath = QProcessEnvironment::systemEnvironment().value(QStringLiteral("APPIMAGE"));
    // Absoluta sökvägen till AppImage
    QFileInfo appImageInfo(appImagePath);
    QString appImageDir = appImageInfo.absolutePath(); // Hämta katalogens sökväg
    QDir dir(appImageDir); // Nu pekar 'dir' på katalogen
#endif
    dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
    QDirIterator it(dir);
    QString language;
    QTranslator translator;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       DISPLAY_NAME, EXECUTABLE_NAME);

    while(it.hasNext()) {
        QString diritem = it.next();

        if(diritem.endsWith(QStringLiteral(".qm"))) {
            language = diritem;
            break;
        }
    }

    if(!language.isEmpty()) {
        if(!translator.load(language)) {
            QMessageBox::critical(nullptr, QStringLiteral("Language error"),
                                  QStringLiteral("Unable to load language"));
        }

        if(!application.installTranslator(&translator)) {
            QMessageBox::critical(nullptr, QStringLiteral("Language error"),
                                  QStringLiteral("Unable to install translation"));
        }

        // There is no language file.
    } else {
        settings.beginGroup(QStringLiteral("Language"));
        QString language = settings.value(QStringLiteral("language")).toString();
        settings.endGroup();

        if(language == "sv_SE") {
            if(translator.load(QStringLiteral(":i18n/_complete_sv_SE.qm"))) {
                if(! application.installTranslator(&translator)) {
                    const QString message(QStringLiteral("Error opening translation file.\nThe program will start in English."));
                    QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, message);
                }
            }
        }
    }

    /** END LANGUAGE **/
#ifdef Q_OS_LINUX
    settings.beginGroup(QStringLiteral("Settings"));
    QString theme = settings.value(QStringLiteral("theme"), QStringLiteral("system")).toString();
    settings.endGroup();

    if(theme == QStringLiteral("dark")) {
        // Ange det mörka temat för Fusion
        QApplication::setStyle(QStyleFactory::create(QStringLiteral("Fusion")));
        QColor darkGray(53, 53, 53);
        QColor gray(128, 128, 128);
        QColor black(25, 25, 25);
        QColor blue(42, 130, 218);
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, darkGray);
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, black);
        darkPalette.setColor(QPalette::AlternateBase, darkGray);
        darkPalette.setColor(QPalette::ToolTipBase, blue);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, darkGray);
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::Link, blue);
        // darkPalette.setColor(QPalette::Link, Qt::red);
        darkPalette.setColor(QPalette::Highlight, blue);
        // darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        darkPalette.setColor(QPalette::HighlightedText, Qt::yellow);
        darkPalette.setColor(QPalette::Active, QPalette::Button, gray.darker());
        darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::WindowText, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::Text, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::Light, darkGray);
        QApplication::setPalette(darkPalette);
    } else {
        QPalette palette(QApplication::palette());
        palette.setColor(QPalette::Window, Qt::white);
        QApplication::setPalette(palette);
    }

#endif // Q_OS_LINUX
#ifdef Q_OS_WINDOWS
    QApplication::setStyle(QStyleFactory::create(QStringLiteral("windows11")));
    // settings.beginGroup(QStringLiteral("Settings"));
    // QString theme = settings.value(QStringLiteral("theme"), QStringLiteral("system")).toString();
    // settings.endGroup();
//     if((QGuiApplication::styleHints()->colorScheme() == Qt::ColorScheme::Dark) && (QSysInfo::productVersion() == "10")) {
#endif // Q_OS_WINDOWS
    Dialog w;
    QObject::connect(&application, &QCoreApplication::aboutToQuit, &w, &Dialog::setEndConfig);
    w.show();
    int slut = application.exec();
    return slut;
}

