//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef PROCESSWORKER_H
#define PROCESSWORKER_H
#include <QCoreApplication>
#include <QProcess>
#include <QFileInfo>
#include <QDir>

class ProcessWorker : public QObject
{
    Q_OBJECT
public:


    explicit ProcessWorker(const QString &path, QObject *parent = nullptr) : QObject(parent), m_path(path) {}

signals:

    void allExtractDone(bool is_success);
    void extractError(QString error);
    void startExtracting();


public slots:


    void doExtract()
    {
        /*****/
        QFile file(m_path);
        QFileInfo fi(file);
        QDir directory(fi.absolutePath() + QStringLiteral("/svtplay-dl"));

// ipac
        if(directory.exists()) {
            // qDebug() << "EXISTS! " << directory;
        }

        QString EXECUTABLE = QCoreApplication::applicationDirPath() + QStringLiteral("/7za.exe");
        QStringList ARG;
        ARG << QStringLiteral("x") << QStringLiteral("-y") << m_path;
        QProcess *process = new QProcess(this);
        process->setProcessChannelMode(QProcess::MergedChannels);
        connect(process, &QProcess::finished, this, [process, this](int exitCode, QProcess::ExitStatus exitStatus) {
            if(exitStatus == QProcess::NormalExit) {
                /***/
                emit allExtractDone(true);
                QFile file(m_path);
                file.remove();
                /***/
            } else {
                emit allExtractDone(false);
            }

            process->deleteLater();
        });

        connect(process, &QProcess::readyReadStandardOutput, this, [this]() {
            emit startExtracting();
        });

        connect(process, &QProcess::errorOccurred, this, [process, this]() {
            emit extractError(process->errorString());
        });

        QString workingdirectory(QFileInfo(m_path).absolutePath());
        process->setWorkingDirectory(workingdirectory);
        process->start(EXECUTABLE, ARG);
    }

private:
    QString  m_path;
};

#endif // PROCESSWORKER_H
