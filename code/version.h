
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,3,8,0
#define VER_FILEVERSION_STR         "2.3.8.0\0"

#define VER_PRODUCTVERSION          2,3,8,0
#define VER_PRODUCTVERSION_STR      "2.3.8"
#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "A program to find and download the latest svtplay-dl."
#define VER_INTERNALNAME_STR        "A program to find and download the latest svtplay-dl."
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2024 - 2025 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "checkversionsvtplaydl.exe"
#define VER_PRODUCTNAME_STR         "Search-svtplay-dl"

#define VER_COMPANYDOMAIN_STR       "gitlab.com/posktomten/checkversionsvtplaydl"

#endif // VERSION_H
