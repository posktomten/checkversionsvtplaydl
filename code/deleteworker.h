//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          download-svtplay-dl
//          Copyright (C) 2024 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#ifndef DELETEWORKER_H
#define DELETEWORKER_H


#include <QDirIterator>

class DeleteWorker  : public QObject
{

    Q_OBJECT

public:

    explicit  DeleteWorker(const QString &pathtodelete, QObject *parent = nullptr) : QObject(parent), m_pathtodelete(pathtodelete) {}


signals:

    void deleteDone(bool);
    void inprogress(QString filename, qint64 filesdeleted);
    // void stopprogressBarLblmessageClear(bool);
    void filesTodelete(qint64 number);



public slots:
    void doDeletePath()
    {
        // QFileInfo fi(m_pathtodelete);
        QDir dir(m_pathtodelete);
        dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
        QDirIterator it(dir, QDirIterator::Subdirectories);
        qint64 total = 0;

        while(it.hasNext()) {
            it.next();
            total ++;
        }

        emit filesTodelete(total);
        QDirIterator it2(dir, QDirIterator::Subdirectories);
        total = 0;

        while(it2.hasNext()) {
            QString diritem = it2.next();
            QFile file(diritem);
            file.remove();
            total ++;
            emit inprogress(file.fileName(), total);
        }

        dir.removeRecursively();
        // emit stopprogressBarLblmessageClear(true);
        emit deleteDone(true);
    }

private:

    QString m_pathtodelete;
};

#endif // DELETEWORKER_H
