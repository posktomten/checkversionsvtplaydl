//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"

#include <QProcess>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QFileDialog>
#include <QApplication>

void Dialog::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), "").toString();
    settings.endGroup();

    if(sp != QStringLiteral("sv_SE")) {
        /***/
        QMessageBox *msgBox = new QMessageBox(this);
        // MyMsgBox *msgBox = new MyMsgBox(this, QSize(430, 100));
        // msgBox->setFont(this->font());
        // QIcon iconYes(QPixmap(QStringLiteral(":/images/restart.png")));
        // QIcon iconLater(QPixmap(QStringLiteral(":/images/later.png")));
        // QIcon iconReject(QPixmap(QStringLiteral(":/images/exit.png")));
        QPushButton  *yesButton = new QPushButton(tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(120, 30));
        QPushButton  *laterButton = new QPushButton(tr("On next start"), msgBox);
        laterButton->setFixedSize(QSize(120, 30));
        QPushButton  *rejectButton = new QPushButton(tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(120, 30));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program needs to be restarted to switch to Swedish."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("sv_SE"));
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + QStringLiteral("/") +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            QProcess p;
#if QT_VERSION < QT_VERSION_CHECK(5, 6, 3)
            p.startDetached(EXECUTE);
#endif
#if QT_VERSION > QT_VERSION_CHECK(5, 6, 2)
            p.setProgram(EXECUTE);
            p.startDetached();
#endif
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("sv_SE"));
            settings.endGroup();
        } else {
            delete msgBox;
            return;
        }
    }
}

void Dialog::english()

{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), "").toString();
    settings.endGroup();

    if(sp != "en_US") {
        /***/
        QMessageBox *msgBox = new QMessageBox(this);
        // MyMsgBox *msgBox = new MyMsgBox(this, QSize(430, 100));
        // msgBox->setFont(this->font());
        // QIcon iconYes(QPixmap(QStringLiteral(":/images/restart.png")));
        // QIcon iconLater(QPixmap(QStringLiteral(":/images/later.png")));
        // QIcon iconReject(QPixmap(QStringLiteral(":/images/exit.png")));
        QPushButton  *yesButton = new QPushButton(tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(120, 30));
        QPushButton  *laterButton = new QPushButton(tr("On next start"), msgBox);
        laterButton->setFixedSize(QSize(120, 30));
        QPushButton  *rejectButton = new QPushButton(tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(120, 30));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program needs to be restarted to switch to English."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("en_US"));
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + QStringLiteral("/") +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            QProcess p;
#if QT_VERSION < QT_VERSION_CHECK(5, 6, 3)
            p.startDetached(EXECUTE);
#endif
#if QT_VERSION > QT_VERSION_CHECK(5, 6, 2)
            p.setProgram(EXECUTE);
            p.startDetached();
#endif
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("en_US"));
            settings.endGroup();
        } else {
            delete msgBox;
            return;
        }

        /***/
    }
}


