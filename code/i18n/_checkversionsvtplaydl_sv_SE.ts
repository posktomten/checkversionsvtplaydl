<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.cpp" line="82"/>
        <source>Checking if there is a new version...</source>
        <translation>Kontrollerar om det finns en ny version...</translation>
    </message>
    <message>
        <source>Please &quot;right click&quot;, &quot;Update...&quot; to update.</source>
        <translation type="vanished">Vänligen &quot;högerklicka&quot;, &quot;Uppdatera...&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="88"/>
        <location filename="../dialog.cpp" line="337"/>
        <source>Please download the new &quot;portable&quot; version.&lt;br&gt;&lt;br&gt;</source>
        <translation>Ladda ner den nya &quot;bärbara&quot; versionen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="85"/>
        <location filename="../dialog.cpp" line="91"/>
        <location filename="../dialog.cpp" line="334"/>
        <location filename="../dialog.cpp" line="340"/>
        <source>Please &quot;Right click&quot;, &quot;Update Search-svtplay-dl...&quot; to update.</source>
        <translation>Vänligen &quot;Högerklicka&quot;, &quot;Uppdatera Search-svtplay-dl...&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="110"/>
        <source>Visit</source>
        <translation>Besök</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="117"/>
        <source>Search for latest svtplay-dl.</source>
        <translation>Sök efter senaste svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="120"/>
        <source>SSL cannot be found</source>
        <translation>SSL kan inte hittas</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="121"/>
        <source>Your operating system does not support SSL.
Unfortunately, it is not possible to contact svtplay-dl.</source>
        <translation>Ditt operativsystem stöder inte SSL.
Tyvärr går det inte att kontakta svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="128"/>
        <source>Download the latest official Linux version of svtplay-dl.</source>
        <translation>Ladda ner den senaste officiella Linux-versionen av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="132"/>
        <source>Download the latest official Windows 32-bit version of svtplay-dl.</source>
        <translation>Ladda ner den senaste officiella Windows 32-bitarsversionen av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="136"/>
        <source>Download the latest official Windows 64-bit version of svtplay-dl.</source>
        <translation>Ladda ner den senaste officiella Windows 64-bitarsversionen av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="143"/>
        <source>Check Search-svtplay-dl for updates...</source>
        <translation>Kolla Search-svtplay-dl för uppdateringar...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Update Search-svtplay-dl...</source>
        <translation>Uppdatera Search-svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="149"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="151"/>
        <source>Create desktop shortcut</source>
        <translation>Skapa genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="153"/>
        <source>Delete desktop shortcut</source>
        <translation>Ta bort genvägen på skrivbordet</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="156"/>
        <source>Delete old version and unzip the new one</source>
        <translation>Ta bort den gamla versionen och packa upp den nya</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="157"/>
        <source>The new version of svtplay-dl will be unpacked.&lt;br&gt;If there is an older version in the &quot;svtplay-dl&quot; folder,&lt;br&gt;the old folder will be deleted&lt;br&gt;before the new version is unpacked.</source>
        <translation>Den nya versionen av svtplay-dl kommer att packas upp.&lt;br&gt;Om det finns en äldre version i mappen &quot;svtplay-dl&quot;,&lt;br&gt;tas den gamla mappen bort&lt;br&gt;innan den nya versionen packas upp.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="165"/>
        <source>Create applications menu shortcut</source>
        <translation>Skapa genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="167"/>
        <source>Delete applications menu shortcut</source>
        <translation>Ta bort genvägen i programmenyn</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="171"/>
        <source>Remove all settings and exit...</source>
        <translation>Ta bort alla inställningar och avsluta...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="174"/>
        <source>Uninstall...</source>
        <translation>Avinstallera...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="378"/>
        <source>This program&apos;s task is to search for and download the latest svtplay-dl from &lt;br&gt;&lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt;.</source>
        <translation>Detta programs uppgift är att söka efter och ladda ner den senaste svtplay-dl från &lt;br&gt;&lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="vanished">Avinstallera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="177"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="179"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <source>This program&apos;s task is to search for the latest svtplay-dl from &lt;br&gt;&lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt; and download.</source>
        <translation type="vanished">Detta programs uppgift är att söka efter den senaste svtplay-dl från &lt;br&gt;&lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt; och ladda ner.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="380"/>
        <source>Search-svtplay-dl uses &lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://www.7-zip.org/&quot;&gt;7-Zip&lt;/a&gt; to unpack the compressed zip files with svtplay-dl.</source>
        <translation>Search-svtplay-dl använder &lt;a style=&quot;text-decoration :none&quot; ; href=&quot;https://www.7-zip.org/&quot;&gt;7-Zip&lt;/a&gt; för att packa upp de komprimerade zip-filerna med svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="182"/>
        <source>Light Theme</source>
        <translation>Ljust tema</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="184"/>
        <source>Dark Theme</source>
        <translation>Mörkt tema</translation>
    </message>
    <message>
        <source>This program&apos;s task is to search for the latest svtplay-dl from &lt;br&gt;&lt;a style=&quot;text-decoration: none&quot; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam/svtplay-dl&lt;/a&gt; and download.</source>
        <translation type="vanished">Det här programmets uppgift är att söka efter den senaste svtplay-dl från &lt;br&gt;&lt;a style=&quot;text-decoration: none&quot; href=&quot;https://github.com/spaam/svtplay-dl&quot;&gt;github.com/spaam /svtplay-dl&lt;/a&gt; och ladda ner.</translation>
    </message>
    <message>
        <source>This program&apos;s task is to search for the latest svtplay-dl from </source>
        <translation type="vanished">Detta programs uppgift är att söka efter den senaste svtplay-dl från </translation>
    </message>
    <message>
        <source> and download.</source>
        <translation type="vanished"> och ladda ner.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>The uninstaller cannot be found, or is not an executable program.</source>
        <translation>Avinstallationsprogrammet kan inte hittas eller är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="482"/>
        <source>Search for the latest version of svtplay-dl</source>
        <translation>Sök efter den senaste versionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="483"/>
        <source>svtplay-dl web server could not be found. No data can be received.
Check your network and check if the svtplay-dl web server is running.</source>
        <translation>svtplay-dl webbserver kunde inte hittas. Inga data kan tas emot.
Kontrollera ditt nätverk och kontrollera om svtplay-dl webbservern är igång.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="532"/>
        <source>The official version and your version</source>
        <translation>Den officiella versionen och din version</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="539"/>
        <source>Official version</source>
        <translation>Officiell version</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="541"/>
        <location filename="../dialog.cpp" line="561"/>
        <source>Your version</source>
        <translation>Din version</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="559"/>
        <source>The official version</source>
        <translation>Den officiella versionen</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="573"/>
        <source>The latest version of svtplay-dl is from</source>
        <translation>Den senaste versionen av svtplay-dl är från</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="574"/>
        <source>You have not specified which version of svtplay-dl you have. If you have the latest version,&lt;br&gt;click &quot;Save&quot;.&lt;br&gt;Or download the latest version.</source>
        <translation>Du har inte angett vilken version av svtplay-dl du har. Om du har den senaste versionen,&lt;br&gt;klickar du på &quot;Spara&quot;.&lt;br&gt;Eller laddar ner den senaste versionen.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="598"/>
        <source>is saved.</source>
        <translation>är sparad.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="601"/>
        <source>The date and time is not valid.</source>
        <translation>Datum och tid är inte giltiga.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="59"/>
        <source>Downloaded</source>
        <translation>Nedladdade</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="74"/>
        <source>Select download folder for </source>
        <translation>Välj nedladdningsmapp för </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="75"/>
        <source>Select</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="76"/>
        <location filename="../language.cpp" line="49"/>
        <location filename="../language.cpp" line="109"/>
        <location filename="../style.cpp" line="51"/>
        <location filename="../style.cpp" line="105"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="98"/>
        <source>Error opening file for write: </source>
        <translation>Fel vid öppning av fil för skrivning: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="106"/>
        <source>Error write to file: </source>
        <translation>Fel vid skriv till fil: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="116"/>
        <source>&lt;br&gt;Click &quot;Save&quot; to save the version information.</source>
        <translation>&lt;br&gt;Klicka på &quot;Spara&quot; för att spara versionsinformationen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="169"/>
        <source>The unpacking is complete.&lt;br&gt;Click &quot;Save&quot; to save the version information.</source>
        <translation>Uppackningen är klar.&lt;br&gt;Klicka på &quot;Spara&quot; för att spara versionsinformationen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="171"/>
        <location filename="../download.cpp" line="194"/>
        <source>Unpacking error</source>
        <translation>Uppackningsfel</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="189"/>
        <source>Starting to unpack...</source>
        <translation>Börjar packa upp...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="201"/>
        <source>Deleting file: </source>
        <translation>Raderar fil: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="215"/>
        <source>Please check the network.</source>
        <translation>Kontrollera nätverket.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="230"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="161"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="192"/>
        <source>Save the date and time of the your latest version of svtplay-dl.</source>
        <translation>Spara datum och tid för din senaste version av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="195"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="220"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="262"/>
        <source>Linux</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="287"/>
        <source>Windows 32</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="312"/>
        <source>Windows 64</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="45"/>
        <location filename="../language.cpp" line="105"/>
        <location filename="../style.cpp" line="47"/>
        <location filename="../style.cpp" line="101"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="47"/>
        <location filename="../language.cpp" line="107"/>
        <location filename="../style.cpp" line="49"/>
        <location filename="../style.cpp" line="103"/>
        <source>On next start</source>
        <translation>Vid nästa start</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="56"/>
        <source>The program needs to be restarted to switch to Swedish.</source>
        <translation>Programmet behöver startas om för att gå över till svenska.</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="116"/>
        <source>The program needs to be restarted to switch to English.</source>
        <translation>Programmet behöver startas om för att gå över till engelsk.</translation>
    </message>
    <message>
        <location filename="../style.cpp" line="58"/>
        <source>The program needs to be restarted to switch to light theme.</source>
        <translation>Programmet måste startas om för att byta till ljust tema.</translation>
    </message>
    <message>
        <location filename="../style.cpp" line="112"/>
        <source>The program needs to be restarted to switch to dark theme.</source>
        <translation>Programmet måste startas om för att byta till mörkt tema.</translation>
    </message>
</context>
<context>
    <name>ProcessWorker</name>
    <message>
        <source>Unable to delete the old folder &quot;svtplay-dl&quot;. Check your file permissions.</source>
        <translation type="vanished">Det gick inte att ta bort den gamla mappen &quot;svtplay-dl&quot;. Kontrollera dina filbehörigheter.</translation>
    </message>
</context>
</TS>
