//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef NETWORKWORKER_H
#define NETWORKWORKER_H
#include <QNetworkReply>

class NetworkWorker : public QObject
{
    Q_OBJECT
public:
    explicit NetworkWorker(const QUrl &url, const QString &filename, QObject *parent = nullptr) : QObject(parent), m_url(url), m_filename(filename) {}

signals:
    void downloadProgress(qint64 a, qint64 b);
    void sendFileForSave(QByteArray filearray, QString filename);
    void allWorkDone();
    void error(QNetworkReply::NetworkError error);

public slots:
    void doDownload()
    {
        QNetworkAccessManager *nam = new QNetworkAccessManager(this);
        QNetworkRequest request(m_url);
        auto *reply = nam->get(request);
        connect(reply, &QNetworkReply::downloadProgress, this, [this, reply](qint64 a, qint64 b) {
            emit downloadProgress(a, b);
        });

        // Hantera när svaret är klart
        connect(reply, &QNetworkReply::finished, this, [this, reply]() {
            if(reply->error() == QNetworkReply::NoError) {
                // Om allt gick bra, läs data och emit resultatet
                QByteArray bytes = reply->readAll();
                emit sendFileForSave(bytes, m_filename);
                emit allWorkDone();
            } else {
                emit allWorkDone();
                // Om ett fel inträffar, emit ett felmeddelande
                QNetworkReply::NetworkError networkerror = reply->error();
                emit error(networkerror); // Skicka felet som värde
            }

            // Rensa upp
            reply->deleteLater();
        });
    }



private:
    QUrl m_url;
    QString  m_filename;

};

#endif // NETWORKWORKER_H
