//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
// Hej
#include "dialog.h"
#include "ui_dialog.h"
#include <QSettings>
#include <QMovie>
#include <QMenu>
#include <QDesktopServices>
#include <QXmlStreamReader>
#include <QStyleHints>
#include <QProcess>
#if defined(Q_OS_WINDOWS)
#include "createshortcut_win.h"
#elif defined(Q_OS_LINUX)
#include "createshortcut_lin.h"
#include "update.h"
#include "updatedialog.h"
#endif
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
#include <QFileInfo>
#include "download_install.h"
#endif
#include "about.h"
#include "checkupdate.h"
#include <Qt>
#include <QStyleHints>
#include <QStyleFactory>


Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup("Geometry");
    this->restoreGeometry(settings.value("savegeometry").toByteArray());
    settings.endGroup();
    ui->label->setFont(QApplication::font());
    //  qDebug() << "*QApplication::style() " << QApplication::style();
    QApplication::setDesktopSettingsAware(true);
#if defined(Q_OS_WINDOWS)

    if(QApplication::styleHints()->colorScheme() == Qt::ColorScheme::Dark) {
        theme = "dark";
    }

#endif
#if defined(Q_OS_LINUX)
    settings.beginGroup(QStringLiteral("Settings"));
    theme = settings.value(QStringLiteral("theme"), QStringLiteral("light")).toString();
    settings.endGroup();

    if(theme == QStringLiteral("dark")) {
        QPalette palette = QApplication::palette();
        palette.setColor(QPalette::Link, Qt::red);
        QApplication::setPalette(palette);
    }

#endif
#if defined(Q_OS_WINDOWS)
    ui->pbDownloadLin->setVisible(false);
#endif
#if defined(Q_OS_LINUX)
    ui->pbDownloadWin32->setVisible(false);
    ui->pbDownloadWin64->setVisible(false);
#endif
    ui->label->setText(tr("Checking if there is a new version..."));
    auto *cfu = new CheckUpdate;
#if defined(Q_OS_LINUX)
    const QString updateinstruktions(tr("Please \"Right click\", \"Update Search-svtplay-dl...\" to update."));
#endif
#if defined(Q_OS_WINDOWS) && defined(PORTABLE)
    const QString updateinstruktions(tr("Please download the new \"portable\" version.<br><br>") + QStringLiteral(" <a style=\"font-weight: bold; text-decoration: none;\" href =\"") + WEBSITE + QStringLiteral("\"> ") + WEBSITE + QStringLiteral("</a>"));
#endif
#if defined(Q_OS_WINDOWS) && !defined(PORTABLE)
    const QString updateinstruktions(tr("Please \"Right click\", \"Update Search-svtplay-dl...\" to update."));
#endif
    QIcon *icon = new QIcon(QStringLiteral(":/resources/checkversionsvtplaydl.png"));
    cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstruktions, icon);
    ui->imageLabel->setAttribute(Qt::WA_NoSystemBackground);
    ui->pbCheck->animateClick();
    QMovie* movie = new QMovie(QStringLiteral(":/resources/search.gif"));

    if(!movie->isValid()) {
    } else {
        movie->setScaledSize(QSize(200, 200));
        ui->imageLabel->setMovie(movie);
        movie->start();
    }

    this->setFixedSize(364, 320);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(*icon);
    this->setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
    ui->cmbSvtplaydl->addItem(tr("Visit"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Snapshots"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Download"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Options"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Supported Sites"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Github"));
    ui->cmbSvtplaydl->addItem(QStringLiteral("Issues"));
    ui->pbCheck->setToolTip(tr("Search for latest svtplay-dl."));

    if(!QSslSocket::supportsSsl()) {
        QMessageBox::critical(nullptr, tr("SSL cannot be found"),
                              tr("Your operating system does not support "
                                 "SSL.\nUnfortunately, it is "
                                 "not possible to contact svtplay-dl."));
        close();
    }

// DOWNLOAD BUTTONS
    ui->pbDownloadLin->setToolTip(tr("Download the latest official Linux version of svtplay-dl."));
    connect(ui->pbDownloadLin, &QPushButton::clicked, this, [this]() {
        download(QString(QStringLiteral("svtplay-dl")));
    });

    ui->pbDownloadWin32->setToolTip(tr("Download the latest official Windows 32-bit version of svtplay-dl."));
    connect(ui->pbDownloadWin32, &QPushButton::clicked, this, [this]() {
        download(QString(QStringLiteral("svtplay-dl-win32.zip")));
    });

    ui->pbDownloadWin64->setToolTip(tr("Download the latest official Windows 64-bit version of svtplay-dl."));
    connect(ui->pbDownloadWin64, &QPushButton::clicked, this, [this]() {
        download(QString(QStringLiteral("svtplay-dl-amd64.zip")));
    });

// CONTEXT MENU
    this->setContextMenuPolicy(Qt::CustomContextMenu);
// Checkupdate
    QAction *actionCheckupdate = new QAction(tr("Check Search-svtplay-dl for updates..."));
    actionCheckupdate->setIcon(QIcon(QStringLiteral(":/resources/checkversionsvtplaydl.png")));
#if !defined(PORTABLE)
    QAction *actionUpdate = new QAction(tr("Update Search-svtplay-dl..."));
    actionUpdate->setIcon(QIcon(QStringLiteral(":/resources/checkversionsvtplaydl.png")));
#endif
    QAction *actionAbout = new QAction(tr("About..."));
    actionAbout->setIcon(QIcon(":/resources/about.png"));
    QAction *actionDesktopChortcut = new QAction(tr("Create desktop shortcut"));
    actionDesktopChortcut->setIcon(QIcon(":/resources/plus.png"));
    QAction *actionDeleteDesktopChortcut = new QAction(tr("Delete desktop shortcut"));
    actionDeleteDesktopChortcut->setIcon(QIcon(QStringLiteral(":/resources/minus.png")));
#if defined(Q_OS_WINDOWS)
    actionExtract = new QAction(tr("Delete old version and unzip the new one"));
    actionExtract->setToolTip(tr("The new version of svtplay-dl will be unpacked.<br>If there is an older version in the \"svtplay-dl\" folder,<br>the old folder will be deleted<br>before the new version is unpacked."));
    actionExtract->setCheckable(true);
// actionExtract->setIcon(QIcon(QStringLiteral(":/resources/7zip.png")));
    settings.beginGroup("Settings");
    actionExtract->setChecked(settings.value("do_extract", false).toBool());
    settings.endGroup();
#endif
#if defined(Q_OS_LINUX) || defined(PORTABLE)
    QAction *actionAplicationsChortcut = new QAction(tr("Create applications menu shortcut"));
    actionAplicationsChortcut->setIcon(QIcon(QStringLiteral(":/resources/plus.png")));
    QAction *actionDeleteAplicationsChortcut = new QAction(tr("Delete applications menu shortcut"));
    actionDeleteAplicationsChortcut->setIcon(QIcon(QStringLiteral(":/resources/minus.png")));
    actionAplicationsChortcut->setIcon(QIcon(QStringLiteral(":/resources/plus.png")));
#endif
    QAction *actionDeleteSettings = new QAction(tr("Remove all settings and exit..."));
    actionDeleteSettings->setIcon(QIcon(QStringLiteral(":/resources/minus.png")));
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
    QAction *actionUninstall = new QAction(tr("Uninstall..."));
    actionUninstall->setIcon(QIcon(QStringLiteral(":/resources/uninstall.png")));
#endif
    QAction *actionEnglish = new QAction(tr("English"));
    actionEnglish->setIcon(QIcon(QStringLiteral(":/resources/english.png")));
    QAction *actionSwedish = new QAction(tr("Swedish"));
    actionSwedish->setIcon(QIcon(QStringLiteral(":/resources/swedish.png")));
#if (defined Q_OS_LINUX)
    QAction *actionLightTheme = new QAction(tr("Light Theme"));
    actionLightTheme->setIcon(QIcon(QStringLiteral(":/resources/light.png")));
    QAction *actionDarkTheme = new QAction(tr("Dark Theme"));
    actionDarkTheme->setIcon(QIcon(QStringLiteral(":/resources/dark.png")));
#endif
#if (defined Q_OS_LINUX)
    QObject::connect(this, &QWidget::customContextMenuRequested, this, [actionDesktopChortcut, actionDeleteDesktopChortcut, actionAplicationsChortcut, actionDeleteAplicationsChortcut, actionAbout, actionCheckupdate, actionUpdate, actionDeleteSettings, actionEnglish, actionSwedish, actionLightTheme, actionDarkTheme]() {
#endif
#if defined(PORTABLE)
        QObject::connect(this, &QWidget::customContextMenuRequested, this, [actionDesktopChortcut, actionDeleteDesktopChortcut, actionAplicationsChortcut, actionDeleteAplicationsChortcut, actionAbout, actionDeleteSettings, actionCheckupdate, actionEnglish, actionSwedish, this]() {
#endif
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
            QObject::connect(this, &QWidget::customContextMenuRequested, this, [actionDesktopChortcut, actionDeleteDesktopChortcut,  actionAbout, actionDeleteSettings, actionUninstall, actionCheckupdate, actionUpdate,  actionEnglish, actionSwedish, this]() {
#endif
                QMenu *aplicationMenu = new QMenu;
                aplicationMenu->setToolTipsVisible(true);
                aplicationMenu->setToolTipDuration(1000);
                aplicationMenu->setFont(QApplication::font());
                aplicationMenu->addAction(actionAbout);
#if defined(PORTABLE) || defined(Q_OS_LINUX)
                aplicationMenu->addSeparator();
                aplicationMenu->addAction(actionAplicationsChortcut);
                aplicationMenu->addAction(actionDeleteAplicationsChortcut);
                aplicationMenu->addSeparator();
#endif
                aplicationMenu->addSeparator();
                aplicationMenu->addAction(actionDesktopChortcut);
                aplicationMenu->addAction(actionDeleteDesktopChortcut);
                aplicationMenu->addSeparator();
                aplicationMenu->addAction(actionCheckupdate);
#if !defined(PORTABLE)
                aplicationMenu->addAction(actionUpdate);
#endif
                aplicationMenu->addAction(actionDeleteSettings);
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
                aplicationMenu->addAction(actionUninstall);
#endif
                aplicationMenu->addSeparator();
#if defined(Q_OS_WINDOWS)
                aplicationMenu->addAction(actionExtract);
#endif
                aplicationMenu->addSeparator();
                aplicationMenu->addAction(actionEnglish);
                aplicationMenu->addAction(actionSwedish);
                aplicationMenu->addSeparator();
#if defined (Q_OS_LINUX)
                aplicationMenu->addAction(actionLightTheme);
                aplicationMenu->addAction(actionDarkTheme);
#endif
                aplicationMenu->exec(QCursor::pos());
                aplicationMenu->clear();
                aplicationMenu->deleteLater();
            });

// END CONTEXT MENU
// ACTIONS FROM CONTEXT MENU
            // language
            /*** LANGUAGE ***/
            connect(actionSwedish, &QAction::triggered, this, &Dialog::swedish);
            connect(actionEnglish, &QAction::triggered, this, &Dialog::english);
#if defined Q_OS_LINUX
            connect(actionDesktopChortcut, &QAction::triggered, []() {
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                QString *comments = new QString(QStringLiteral(DISPLAY_NAME));
                QString *categories = new QString(QStringLiteral(CATEGORIES));
                QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
                Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, false, true);
            });

            connect(actionDeleteDesktopChortcut, &QAction::triggered, []() {
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::removeDesktopShortcut(executable_name);
            });

            connect(actionAplicationsChortcut, &QAction::triggered, []() {
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                QString *comments = new QString(QStringLiteral(DISPLAY_NAME));
                QString *categories = new QString(QStringLiteral(CATEGORIES));
                QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
                Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
            });

            connect(actionDeleteAplicationsChortcut, &QAction::triggered, []() {
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::removeApplicationShortcut(executable_name);
            });

            QObject::connect(actionUpdate, &QAction::triggered, this, [this]() {
                QPixmap *pixmap = new QPixmap(QStringLiteral(":/resources/checkversionsvtplaydl.png"));
                Update *up = new Update(nullptr);
                // connect(up, &Update::isUpdated, this, &Dialog::isUpdated);
                ud = new UpdateDialog(nullptr);
                ud->viewUpdate(pixmap);
                ud->show();
                up->doUpdate(ARG1, ARG2, DISPLAY_NAME, ud, pixmap);
                close();
            });

#endif
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
            QObject::connect(actionUpdate, &QAction::triggered, this, [this]() {
                DownloadInstall *mDownloadInstall = new DownloadInstall(nullptr);
                // mDownloadInstall = new DownloadInstall;
                QString *path = new QString(QStringLiteral(PATH));
                QString *filename = new QString(QStringLiteral(FILENAME));
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *version = new QString(QStringLiteral(VERSION));
                mDownloadInstall->setValues(path, filename, display_name, version);
                mDownloadInstall->show();
                close();
            });

#endif
#if defined(Q_OS_WINDOWS)
            // DELETA ALL SETTINGS
            connect(actionDeleteSettings, &QAction::triggered, this, [this]() {
                close();
                QString display_name(QStringLiteral(DISPLAY_NAME));
                // QString executable_name(QStringLiteral(EXECUTABLE_NAME));
                QString version(QStringLiteral(VERSION));
                // Createshortcut::removeDesktopShortcutSilent(&display_name);
                // Createshortcut::removeApplicationShortcutSilent(&display_name);
                // Createshortcut::deleteSettingsSilent(&display_name, &executable_name, &version);
#if defined(PORTABLE)
                bool download_install = false;
#else
                bool download_install = false;
#endif
                Createshortcut::deleteAllSettings(&display_name, &display_name, &version, download_install);
            });

            connect(actionExtract, &QAction::triggered, this, [this]() {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                settings.beginGroup("Settings");

                if(actionExtract->isChecked()) {
                    settings.setValue("do_extract", true);
                }  else {
                    settings.setValue("do_extract", false);
                }

                settings.endGroup();
            });

#endif
#if defined(Q_OS_LINUX)
            connect(actionDeleteSettings, &QAction::triggered, this, [this]() {
                close();
                QIcon *icon = new QIcon(QStringLiteral(":/resources/checkversionsvtplaydl.png"));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *version = new QString(QStringLiteral(VERSION));
                Createshortcut::deleteAllSettings(display_name, executable_name, version, icon);
            });

#endif
            connect(actionCheckupdate, &QAction::triggered, this, []() {
                auto *cfu = new CheckUpdate;
#if defined(Q_OS_LINUX)
                const QString updateinstruktions(tr("Please \"Right click\", \"Update Search-svtplay-dl...\" to update."));
#endif
#if defined(Q_OS_WINDOWS) && defined(PORTABLE)
                const QString updateinstruktions(tr("Please download the new \"portable\" version.<br><br>") + QStringLiteral(" <a style=\"font-weight: bold; text-decoration: none;\" href =\"") + WEBSITE + QStringLiteral("\"> ") + WEBSITE + QStringLiteral("</a>"));
#endif
#if defined(Q_OS_WINDOWS) && !defined(PORTABLE)
                const QString updateinstruktions(tr("Please \"Right click\", \"Update Search-svtplay-dl...\" to update."));
#endif
                QIcon *icon = new QIcon(QStringLiteral(":/resources/checkversionsvtplaydl.png"));
                cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstruktions, icon);
            });

#if defined Q_OS_WIN
#if defined(PORTABLE)
            connect(actionAplicationsChortcut, &QAction::triggered, []() {
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::makeShortcutFile(display_name, executable_name, true, false);
            });

            connect(actionDeleteAplicationsChortcut, &QAction::triggered, [this]() {
                QString display_name = QStringLiteral(DISPLAY_NAME);
                // QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::removeApplicationShortcutSilent(&display_name);
            });

#endif
            connect(actionDesktopChortcut, &QAction::triggered, []() {
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::makeShortcutFile(display_name, executable_name, false, true);
            });

            connect(actionDeleteDesktopChortcut, &QAction::triggered, []() {
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                // QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::removeDesktopShortcutSilent(display_name);
            });

#endif
#if defined (Q_OS_LINUX)
            connect(actionLightTheme, &QAction::triggered, this, [this]() {
                light();
            });

            connect(actionDarkTheme, &QAction::triggered, this, [this]() {
                dark();
            });

#endif
            connect(actionAbout, &QAction::triggered, this, []() {
                const QString *purpose = new QString(tr("This program's task is to search for and download the latest svtplay-dl from <br><a style=\"text-decoration :none\" ; href=\"https://github.com/spaam/svtplay-dl\">github.com/spaam/svtplay-dl</a>."));
#if defined(Q_OS_WINDOWS)
                const QString *translator = new QString(tr("Search-svtplay-dl uses <a style=\"text-decoration :none\" ; href=\"https://www.7-zip.org/\">7-Zip</a> to unpack the compressed zip files with svtplay-dl."));
#else
                const QString *translator = new QString("");
#endif
                const QPixmap *pixmap = new QPixmap(QStringLiteral(":/resources/checkversionsvtplaydl.png"));
//        const QString *copyright_year = new QString(COPYRIGHT_YEAR);
                const QString *copyright_year = new QString(COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year()));
//      const QPixmap *pixmap = nullptr;
                About *about = new About(QApplication::font(),
                                         DISPLAY_NAME,
                                         DISPLAY_VERSION,
                                         COPYRIGHT,
                                         EMAIL,
                                         copyright_year,
                                         BUILD_DATE_TIME,
                                         LICENSE,
                                         LICENSE_LINK,
                                         CHANGELOG,
                                         SOURCECODE,
                                         WEBSITE,
                                         COMPILEDON,
                                         purpose,
                                         translator,
                                         pixmap,
                                         false);
                delete about;
            });

            // connect(actionDeleteSettings, &QAction::triggered, [this]() {
            //     deleteAllSettings();
            // });
#if !defined(PORTABLE) && defined(Q_OS_WINDOWS)
            connect(actionUninstall, &QAction::triggered, this, [this]() {
                close();
                QString display_name(QStringLiteral(DISPLAY_NAME));
                QString executable_name(QStringLiteral(EXECUTABLE_NAME));
                QString version(QStringLiteral(VERSION));
                Createshortcut::removeDesktopShortcutSilent(&display_name);
                Createshortcut::removeApplicationShortcutSilent(&display_name);
                Createshortcut::deleteSettingsSilent(&display_name, &executable_name, &version);
                /*****/
                QString EXECUTABLE = (QCoreApplication::applicationDirPath() + QStringLiteral("/uninstall.exe"));
                QFile file(EXECUTABLE);
                QFileInfo fi(file);

                if(fi.exists() && fi.isExecutable()) {
                    QProcess p;
                    p.setProgram(EXECUTABLE);
                    p.startDetached();
                    close();
                    return;
                } else {
                    QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                                          tr("The uninstaller cannot be found, or is not an executable program."));
                }
            });

#endif // !defined(PORTABLE) && defined(Q_OS_WINDOWS)
// END ACTIONS FROM CONTEXT MENU
            QString *ny = new QString;
            // QClipboard *clipboard = QApplication::clipboard();
            connect(ui->pbCheck, &QPushButton::clicked, this, [this, ny]() -> void {

                QMovie* movie = new QMovie(QStringLiteral(":/resources/spinner.gif"));
                //    movie->setSpeed(200); // 2x speed

                // Make sure the GIF was loaded correctly
                if(!movie->isValid()) {
                } else {
                    movie->setScaledSize(QSize(100, 100));
                    ui->imageLabel->setMovie(movie);
                    movie->start();
                }
                QUrl url(URL_SNAPSHOTS);

                auto *nam = new QNetworkAccessManager(nullptr);

                auto *reply = nam->get(QNetworkRequest(url));

                QObject::connect(
                reply, &QNetworkReply::finished, this, [this, reply, ny]() -> void {
                    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                       DISPLAY_NAME, EXECUTABLE_NAME);

                    settings.beginGroup(QStringLiteral("DateTime"));
                    QString old = settings.value(QStringLiteral("datetime"), QStringLiteral("EMPTY")).toString();
                    settings.endGroup();
                    /*   */
                    QByteArray bytes = reply->readAll(); // bytes
                    QString newVersion(bytes);           // string

                    QXmlStreamReader xml(newVersion);
                    QString textString = QString("");

                    while(!xml.atEnd()) {
                        if(xml.readNext() == QXmlStreamReader::Characters) {
                            textString += xml.text();
                        }
                    }
                    if(textString.isEmpty())
                    {
                        QMessageBox::critical(
                            nullptr, tr("Search for the latest version of svtplay-dl"),
                            tr("svtplay-dl web server could not be found. No data can be "
                               "received.\nCheck your network and check if the svtplay-dl "
                               "web server is running."));
                        ui->pbDownloadLin->setDisabled(true);
                        ui->pbDownloadWin32->setDisabled(true);
                        ui->pbDownloadWin64->setDisabled(true);
                        return;
                    }

                    QStringList rader = textString.split('\n');

                    int sistarad = rader.size();
                    *ny = rader.at(sistarad - 2);
                    downloadpath = ny->left(15);
                    *ny = ny->remove('_');
                    *ny = ny->left(14);

                    ny->insert(4, '-');
                    ny->insert(7, '-');
                    ny->insert(10, ' ');
                    ny->insert(13, ':');
                    ny->insert(16, ':');

                    QString utn = *ny;
                    latestversion = utn;

//            utn = utn.replace('T', ' ');
                    QString uto = old;


                    if(old != "EMPTY")
                    {
                        uto = uto.replace('T', ' ');
                        QDateTime dold = QDateTime::fromString(old, Qt::ISODate);
                        QDateTime dny = QDateTime::fromString(*ny, Qt::ISODate);

                        if(dny == dold) {
                            QMovie *movie = new QMovie(QStringLiteral(":/resources/thumbs-up.gif"));
                            movie->setSpeed(200); // 2x speed
                            movie->setScaledSize(QSize(150, 150));
                            ui->imageLabel->setMovie(movie);
                            movie->start();

                            if(theme == "dark") {
                                ui->label->setStyleSheet(QStringLiteral("QLabel { color : rgb(0,204,0); }"));
                            } else {
                                ui->label->setStyleSheet(QStringLiteral("QLabel { color : rgb(0,102,0); }"));
                            }

                            ui->label->setText(tr("The official version and your version") + "<br><b>" + uto + "</b>");
                        } else if(dny > dold) {
                            QMovie *movie = new QMovie(":/resources/update.gif");
                            movie->setScaledSize(QSize(200, 200));
                            ui->imageLabel->setMovie(movie);
                            movie->start();
                            ui->label->setStyleSheet(QStringLiteral("QLabel { color : red; }"));
                            ui->label->setText(tr("Official version") + "<br><b>" +
                                               utn + "</b><br><br>" +
                                               tr("Your version") + "<br><b>" +
                                               uto + "</b>");
                        } else {
                            QMovie* movie = new QMovie(QStringLiteral(":/resources/astonished.gif"));

                            if(!movie->isValid()) {
                            } else {
                                movie->setScaledSize(QSize(130, 130));
                                ui->imageLabel->setMovie(movie);
                                movie->start();
                            }

                            if(theme == "dark") {
                                ui->label->setStyleSheet(QStringLiteral("QLabel { color : white; }"));
                            } else {
                                ui->label->setStyleSheet(QStringLiteral("QLabel { color : blue; }"));
                            }

                            ui->label->setText(tr("The official version") + "<br><b>" +
                                               utn + "</b><br><br>" +
                                               tr("Your version") + "<br><b>" +
                                               uto + "</b>");
                        }
                    } else
                    {
                        if(theme == "dark") {
                            ui->label->setStyleSheet(QStringLiteral("QLabel { color : white; }"));
                        } else {
                            ui->label->setStyleSheet(QStringLiteral("QLabel { color : black; }"));
                        }

                        ui->label->setText(
                            tr("The latest version of svtplay-dl is from") + "<br><b>" + utn + "</b><br>" +
                            tr("You have not specified which version of svtplay-dl you have. If you have the latest version,<br>click \"Save\".<br>Or download the latest version."));
                    }
                });
                // ui->pbCopy->setEnabled(true);
            });
            connect(ui->pbExit, &QPushButton::clicked, this, [this]() {
                close();
            });

            connect(ui->pbSet, &QPushButton::clicked, this, [this]() {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                QString text = latestversion;

                if(!text.isEmpty()) {
                    static QRegularExpression re(QStringLiteral("20\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d"));
                    // 2020-06-02T15:41:29
                    QRegularExpressionMatch match = re.match(text);
                    bool hasMatch = match.hasMatch();
                    QDateTime dt = QDateTime::fromString(text, Qt::ISODate);

                    if((dt.isValid()) && (hasMatch) && (text.size() == 19)) {
                        settings.beginGroup(QStringLiteral("DateTime"));
                        settings.setValue(QStringLiteral("datetime"), text);
                        settings.endGroup();
                        ui->pbSet->setToolTip("\"" + text + "\" " + tr("is saved."));
                    } else {
                        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION,
                                              tr("The date and time is not valid."));
                    }
                }

                ui->pbCheck->animateClick();
            });

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
            connect(ui->cmbSvtplaydl, &QComboBox::textActivated,
#endif
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
                    connect(ui->cmbSvtplaydl, &QComboBox::currentIndexChanged, this,
#endif
            [this]() -> void {
                switch(ui->cmbSvtplaydl->currentIndex()) {
                    case 1:
                        QDesktopServices::openUrl(QUrl(URL_SNAPSHOTS));
                        break;

                    case 2:
                        QDesktopServices::openUrl(QUrl(URL_DOWNLOAD));
                        break;

                    case 3:
                        QDesktopServices::openUrl(QUrl(URL_OPTIONS));
                        break;

                    case 4:
                        QDesktopServices::openUrl(QUrl(URL_SUPORTED_SITES));
                        break;

                    case 5:
                        QDesktopServices::openUrl(QUrl(URL_GITHUB));
                        break;

                    case 6:
                        QDesktopServices::openUrl(QUrl(URL_ISSUES));
                        break;
                }

            });
        }

        Dialog::~Dialog() {
            delete ui;
        }

        void Dialog::setEndConfig() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Geometry");
            settings.setValue("savegeometry", this->saveGeometry());
            settings.endGroup();
            settings.sync();
        }
