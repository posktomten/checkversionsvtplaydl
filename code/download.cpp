//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QThread>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>
#include "dialog.h"
#if defined(Q_OS_WINDOWS)
#include "processworker.h"
#include "deleteworker.h"
#endif
#include "ui_dialog.h"
#include "networkworker.h"
#if defined(Q_OS_LINUX)

#endif
void Dialog::download(QString filename)
{
    QString todownload = URL_SNAPSHOTS + downloadpath + "/" + filename;
    QThread *thread = new QThread;
    NetworkWorker *worker = new NetworkWorker(todownload,  filename);
    worker->moveToThread(thread);
    connect(thread, &QThread::started, worker, &NetworkWorker::doDownload);
    connect(worker, &NetworkWorker::downloadProgress, this, &Dialog::downloadProgress);
    connect(worker, &NetworkWorker::error, this, &Dialog::error);
    connect(worker, &NetworkWorker::sendFileForSave, this, &Dialog::getFileForSave);
    connect(worker, &NetworkWorker::allWorkDone, this, [thread, worker]() {
        // Rensa upp
        thread->quit();
        thread->wait();  // Vänta på att tråden är helt avslutad
        // Rensa upp
        thread->deleteLater();
        worker->deleteLater();
    });

    thread->start();
}

//public slots:
void Dialog::downloadProgress(qint64 a, qint64 b)
{
    qint64 procent = a / b * 100;
    ui->label->setText(QString::number(procent) + "% " + tr("Downloaded"));
}

void Dialog::getFileForSave(QByteArray filearray, QString filename)
{
    // Skapa QFileDialog-instans
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Path"));
    QString downloadpath = settings.value(QStringLiteral("downloadpath"), QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
    settings.endGroup();
    QFileDialog dialog(nullptr);
    dialog.setDirectory(downloadpath); // Ställ in standardvägen
    dialog.setFileMode(QFileDialog::Directory); // Endast mappar
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setWindowTitle(tr("Select download folder for ") + filename);
    dialog.setLabelText(QFileDialog::Accept, tr("Select"));
    dialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    // Anpassa storlek och position (valfritt)
    dialog.setGeometry(this->x() + 50, this->y() + 50, 800, 550);
    // dialog.setGeometry(0, 0, 900, 550);

    // Visa dialogen
    if(dialog.exec() == QDialog::Rejected) {
        return; // Användaren avbröt
    }

    // Hämta den valda mappen
    QString path = dialog.selectedFiles().at(0); // Första filen är den valda mappen
    settings.beginGroup(QStringLiteral("Path"));
    settings.setValue(QStringLiteral("downloadpath"), path);
    settings.endGroup();
    settings.sync();
    QFile file(path + "/" + filename);

    // Spara den valda sökvägen för framtida användning

    // Försök öppna filen för skrivning
    if(!file.open(QIODevice::WriteOnly)) {
        QString errormessage = tr("Error opening file for write: ") + file.errorString();
        messageBoxCritical(errormessage);
        file.close();
        return;
    }

    // Skriv data till filen
    if(file.write(filearray) == -1) {
        QString errormessage = tr("Error write to file: ") + file.errorString();
        messageBoxCritical(errormessage);
        file.close();
        return;
    }

    file.close();
#if defined(Q_OS_LINUX)
    QFileInfo fi(file);
    QFile::setPermissions(fi.absoluteFilePath(), QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ExeOwner | QFileDevice::ReadGroup | QFileDevice::ExeGroup | QFileDevice::ReadOther | QFileDevice::ExeOther);
    ui->label->setText(ui->label->text() + tr("<br>Click \"Save\" to save the version information."));
}

#endif
#if defined(Q_OS_WINDOWS)
settings.beginGroup(QStringLiteral("Settings"));
bool do_extract = settings.value(QStringLiteral("do_extract"), false).toBool();
settings.endGroup();

if(filename.endsWith(QStringLiteral(".zip")))
{
    if(do_extract) {
        QString pathtodelete(path + QStringLiteral("/svtplay-dl"));
        QDir dir(pathtodelete);

        if(dir.exists()) {
            QThread *thread = new QThread;
            DeleteWorker *delworker = new DeleteWorker(pathtodelete);
            delworker->moveToThread(thread);
            connect(thread, &QThread::started, delworker, &DeleteWorker::doDeletePath);
            connect(delworker, &DeleteWorker::inprogress, this, &Dialog::inprogress);
            connect(delworker, &DeleteWorker::filesTodelete, this, &Dialog::filesTodelete);
            // connect(worker, &DeleteWorker::stopprogressBarLblmessageClear, this, &Dialog::stopprogressBarLblmessageClear);
            connect(delworker, &DeleteWorker::deleteDone, this, [ thread, delworker, pathtodelete, this, filename, path](bool done) {
                if(done) {
                    // Avsluta trådens eventloop
                    thread->quit();
                    thread->wait();  // Vänta på att tråden är helt avslutad
                    // Rensa upp
                    thread->deleteLater();
                    delworker->deleteLater();
                    extract(path + "/" + filename);
                }
            });

            thread->start();
        } else {
            extract(path + "/" + filename);
        }
    }
}
}

void Dialog::extract(QString toExtract)
{
    QThread *thread = new QThread;
    ProcessWorker *processworker = new ProcessWorker(toExtract);
    processworker->doExtract();
    processworker->moveToThread(thread);
    connect(processworker, &ProcessWorker::startExtracting, this, &Dialog::extractingStarted);
    connect(processworker, &ProcessWorker::extractError, this, &Dialog::extractError);
    connect(processworker, &ProcessWorker::allExtractDone, this, [thread, processworker, this](bool sucsess) {
        if(sucsess) {
            ui->label->setText(tr("The unpacking is complete.<br>Click \"Save\" to save the version information."));
        } else {
            messageBoxCritical(tr("Unpacking error"));
        }

        // Rensa upp
        thread->quit();
        thread->wait();  // Vänta på att tråden är helt avslutad
        // Rensa upp
        thread->deleteLater();
        processworker->deleteLater();
    });

    thread->start();
}


// slots (extract)
void Dialog::extractingStarted()
{
    ui->label->setText(tr("Starting to unpack..."));
}

void Dialog::extractError(QString error)
{
    messageBoxCritical(tr("Unpacking error") + QStringLiteral("<br>") + error);
}

// slots (delete)
void Dialog::inprogress(QString filename, qint64 todelete)
{
    filename = "..." + filename.right(50);
    ui->label->setText(tr("Deleting file: ") + filename + QStringLiteral("<br>") + QString::number(todelete) + QStringLiteral(" / ") + QString::number(totalfiles));
}

// slots (delete)
void Dialog::filesTodelete(qint64 antal)
{
    totalfiles = antal;
}

#endif
// slots: (download)
void Dialog::error(QNetworkReply::NetworkError error)
{
    QString errorstring = networkErrorMessages(error);
    messageBoxCritical(tr("Please check the network.") + QStringLiteral("<br>") + errorstring);
}


void Dialog::messageBoxCritical(const QString & text)
{
    static bool isShowing = false;

    if(!isShowing) {
        isShowing = true;
        QMessageBox *msgBox = new QMessageBox(nullptr);
        msgBox->setAttribute(Qt::WA_DeleteOnClose);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(text);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->exec();
        isShowing = false;
    }
}
