//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkversionsvtplaydl
//          Copyright (C) 2020 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/checkversionsvtplaydl
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef PROCESSWINDOWSVERSIONWORKER_H
#define PROCESSWINDOWSVERSIONWORKER_H
#include <QCoreApplication>
#include <QProcess>
#include <QFileInfo>
#include <QDir>

class ProcessWindowsversionWorker : public QObject
{
    Q_OBJECT
public:


    explicit ProcessWindowsversionWorker(QObject *parent = nullptr) : QObject(parent) {}

signals:

    void allDone(QString versionstring);
    void errorversion(QString errormessage);
    void start();


public slots:


    void doWork()
    {
        // QString EXECUTABLE = "C:\\Windows\\System32\\ver";
        QString EXECUTABLE = "C:/Windows/System32/cmd.exe";
        // QString EXECUTABLE = "ver.exe";
        QStringList ARG;
        // ARG << "systeminfo | findstr /E \"OS Name:\"";
        // ARG << "systeminfo | findstr \"OS Name:\"";
        ARG << "ver";
        QProcess *process = new QProcess(nullptr);
        process->setProcessChannelMode(QProcess::MergedChannels);
        connect(process, &QProcess::finished, this, [process, this](int exitCode, QProcess::ExitStatus exitStatus) {
            qDebug() << "KLART!! ";

            if(exitStatus == QProcess::NormalExit) {
                QString versionstring = process->readAllStandardOutput();
                /***/
                qDebug() << "versionstring " << versionstring;
                emit allDone(versionstring);
                /***/
            } else {
                emit allDone(QString("Error read"));
                qDebug() << "error error";
            }

            process->deleteLater();
        });

        connect(process, &QProcess::readyReadStandardOutput, this, [this]() {
            emit start();
        });

        connect(process, &QProcess::errorOccurred, this, [process, this]() {
            QString errormessage = process->errorString();
            emit errorversion(errormessage);
        });

        // QString workingdirectory(QFileInfo(m_path).absolutePath());
        // process->setWorkingDirectory(workingdirectory);
        process->start(EXECUTABLE, ARG);
    }


};

#endif // PROCESSWINDOWSVERSIONWORKER_H
