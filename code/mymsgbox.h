//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef MYMSGBOX_H
#define MYMSGBOX_H

#include <QMessageBox>
#include <QTimer>

class MyMsgBox : public QMessageBox
{
    Q_OBJECT
public:
    explicit MyMsgBox(QWidget *parent = nullptr, const QSize &size = QSize(500, 500)) : QMessageBox(parent), m_size(size)
    {


        QFont font(this->font());
        font.setPointSize(10);
        this->setFont(font);
    }

protected:
    void showEvent(QShowEvent *event) override
    {
        QTimer::singleShot(0, this, [this]() {
            this->setFixedSize(m_size);
        });
    }

private:
    QSize m_size;
};


#endif // MYMSGBOX_H
