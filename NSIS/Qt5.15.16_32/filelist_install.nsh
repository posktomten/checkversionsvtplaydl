;Skapad av nsisFileList 1.3.0
;Sun Jan 5 16:19:35 2025
;Copyright (C) 2022 - 2025 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "icon.ico"
file "libcrypto-3.dll"
file "libgcc_s_dw2-1.dll"
file "libssl-3.dll"
file "libstdc++-6.dll"
file "libwinpthread-1.dll"
file "msvcr100.dll"
file "Qt5Core.dll"
file "Qt5Gui.dll"
file "Qt5Network.dll"
file "Qt5PrintSupport.dll"
file "Qt5Svg.dll"
file "Qt5Widgets.dll"
file "template.exe"
file "unicon.ico"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\License"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\printsupport"
file "printsupport\windowsprintersupport.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qwindowsvistastyle.dll"

SetOutPath "$INSTDIR\bearer"
file "bearer\qgenericbearer.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"
