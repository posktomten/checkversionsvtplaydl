;Delete files
Delete "$INSTDIR\*.*"
Delete "$INSTDIR\imageformats\*.*"
Delete "$INSTDIR\License\*.*"
Delete "$INSTDIR\platforms\*.*"
Delete "$INSTDIR\printsupport\*.*"
Delete "$INSTDIR\styles\*.*"
Delete "$INSTDIR\bearer\*.*"
Delete "$INSTDIR\iconengines\*.*"

;Remove installation folders
RMDir "$INSTDIR\imageformats"
RMDir "$INSTDIR\License"
RMDir "$INSTDIR\platforms"
RMDir "$INSTDIR\printsupport"
RMDir "$INSTDIR\styles"
RMDir "$INSTDIR\bearer"
RMDir "$INSTDIR\iconengines"
RMDir "$INSTDIR"
