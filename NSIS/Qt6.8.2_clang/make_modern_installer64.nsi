﻿
;General
Unicode true
!include MUI2.nsh

!define MUI_BGCOLOR ffffff
!define MUI_TEXTCOLOR "SYSCLR:WindowText"

  
BrandingText "Copyright (C) Ingemar Ceicer 2020-2025"


;SetCompress off
SetCompressor /SOLID lzma


!include "lang\Italian.nsh"
!include "lang\Swedish.nsh"
!include "lang\English.nsh"


  !define PROGNAME "checkversionsvtplaydl"
  !define INSTDIR "Search-svtplay-dl"
  !define FINISHPAGE_LINK "gitlab.com/posktomten/checkversionsvtplaydl/-/wikis/home"
  !define FINISHPAGE_LINK_LOCATION "https://gitlab.com/posktomten/checkversionsvtplaydl/-/wikis/home"
 
  !define VERSION "2.3.8"
  !define PUBLICER "Ingemar Ceicer"
  !define COPYRIGHT "(C) Ingemar Ceicer 2020 - 2025"
  Name "${INSTDIR} ${VERSION}"

; Startar efter installationen
 !define MUI_FINISHPAGE_RUN "$INSTDIR\${EXECUTABLE}"
 !define MUI_FINISHPAGE_RUN_TEXT "Run ${EXECUTABLE} now"


  ;Name and file
  


  !define OUTFILE "${PROGNAME}_64-bit_setup.exe"
  !define EXECUTABLE "${PROGNAME}.exe"
 
  OutFile ${OUTFILE}
  
	InstallDir "$PROGRAMFILES64\${INSTDIR}"

 
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\$(^Name)" ""
 
  ;Define uninstaller name
  !define UninstName "uninstall"

 ; !define MUI_COMPONENTSPAGE_NODESC
  
Function UninstallPrevious

    ; Check for uninstaller.
    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${INSTDIR}\" "UninstallString"
	                     

    ${If} $R0 == ""        
        Goto Done
    ${EndIf}


	CreateDirectory "$PROGRAMFILES64\${INSTDIR}\TEMP" 
	; Make sure plugins do not conflict with a old uninstaller 

   CopyFiles /SILENT /FILESONLY "$PROGRAMFILES64\${INSTDIR}\uninstall.exe" "$PROGRAMFILES64\${INSTDIR}\TEMP"

  ## Silent uninstall
  ; ExecWait '"$PROGRAMFILES64\${INSTDIR}\TEMP\uninstall.exe" /S _?=$PROGRAMFILES64\${INSTDIR}'
  ## NOT silent uninstall
  ExecWait '"$PROGRAMFILES64\${INSTDIR}\TEMP\uninstall.exe" _?=$PROGRAMFILES64\${INSTDIR}'
	 
  Delete "$PROGRAMFILES64\${INSTDIR}\TEMP\*.*"
   RMDir "$PROGRAMFILES64\${INSTDIR}\TEMP"
   RMDir "$PROGRAMFILES64\${INSTDIR}"
    Done:

FunctionEnd
 
;Variables
 
  Var StartMenuFolder

 
;--------------------------------
; DetailsButtonText 
VIProductVersion "${VERSION}.0"

VIAddVersionKey "ProductVersion" "${VERSION}.0"
VIFileVersion "${VERSION}.0"
VIAddVersionKey "FileVersion" "${VERSION}.0"

VIAddVersionKey "ProductName" "${INSTDIR}"

VIAddVersionKey "CompanyName" "${PUBLICER}"
VIAddVersionKey "LegalCopyright" "${COPYRIGHT}"
VIAddVersionKey "FileDescription" "Offline Installer for ${INSTDIR} (64-bit)"

VIAddVersionKey "InternalName" "${INSTDIR}\"
VIAddVersionKey "OriginalFilename" ${OUTFILE}


;--------------------------------
;Interface Settings
  !define MUI_HEADER_TRANSPARENT_TEXT
  !define MUI_ICON "icon.ico"
  !define MUI_UNICON "unicon.ico"
  

 
  !define MUI_ABORTWARNING
  ## ipac
  ;!define MUI_FINISHPAGE_NOAUTOCLOSE
  

  
  !define MUI_WELCOMEFINISHPAGE_BITMAP "icon_large.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "icon_large.bmp"
  
  

 
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_RIGHT
  
  !define MUI_HEADERIMAGE_BITMAP "icon_small.bmp"
 
  !define MUI_FINISHPAGE_LINK ${FINISHPAGE_LINK}
  !define MUI_FINISHPAGE_LINK_LOCATION ${FINISHPAGE_LINK_LOCATION}
 

    InstType "Full" /NOCUSTOM
   ;InstType "Minimal" /NOCUSTOM
   ;InstType "Custom"
   ;InstType $(INST_TYPE_FULL) /NOCUSTOM
   ;InstType $(INST_TYPE_MINIMAL) /NOCUSTOM
   ;InstType $(INST_TYPE_CUSTOM)
   
   ## hide|show|nevershow
  ShowInstDetails hide
  ShowUninstDetails hide

 
  ;Remember the installer language and select it by default
  ;(should be set before installation page)
  !define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
  !define MUI_LANGDLL_REGISTRY_KEY "Software\$(^Name)" 
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"
 
  ;Always show the language selection dialog (override the stored value)
  !define MUI_LANGDLL_ALWAYSSHOW
 
  ;Don't filter languages according to their codepage
  ;!define MUI_LANGDLL_ALLLANGUAGES
 
;--------------------------------
;Macros
 
!macro MUI_FINISHPAGE_SHORTCUT
 
  !ifndef MUI_FINISHPAGE_NOREBOOTSUPPORT
    !define MUI_FINISHPAGE_NOREBOOTSUPPORT
    !ifdef MUI_FINISHPAGE_RUN
      !undef MUI_FINISHPAGE_RUN
    !endif
  !endif
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableCancelButton
  !insertmacro MUI_PAGE_FINISH
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableBackButton
 
  Function DisableCancelButton
 
    EnableWindow $mui.Button.Cancel 0
 
  FunctionEnd
 
  Function DisableBackButton
 
    EnableWindow $mui.Button.Back 0
 
  FunctionEnd
 
!macroend
 
#!macro NextCD Label IDFile
 
#  IfFileExists "${IDFile}" +7
#  MessageBox MB_OK|MB_ICONINFORMATION "$(NextCD) ${Label}..."
 
#  IfFileExists "${IDFile}" +5
#  Sleep 1000
 
#  StrCpy $0 "${IDFile}"
#  MessageBox MB_RETRYCANCEL|MB_ICONEXCLAMATION "$(CDNotFound)" IDRETRY -3
#  Quit
 
#!macroend
 
!include "FileFunc.nsh"
 
!macro Extract7z Label Archive Part
 
  !insertmacro NextCD "${Label}" "${Archive}"
  ${GetFileName} "${Archive}" $0
  DetailPrint "$(Extract) $0... ${Part}"
  Nsis7z::ExtractWithDetails "${Archive}" "$(Extract) $0... %s"
 
!macroend
 
!macro SHORTCUTS Name File Icon
 
  !if "${Name}" == ""
    !undef Name
    !define Name "$(^Name)"
  !endif
  !ifdef UninstName
    StrCpy $1 "${UninstName}.exe"
  !else
    StrCpy $1 "uninstall.exe"
  !endif
 
  ;Create uninstaller
  WriteUninstaller "$OUTDIR\$1"
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder from registry if available
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
 
  ;Create shortcuts
  
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${INSTDIR}}.lnk" "$OUTDIR\${File}" "" "$INSTDIR\icon.ico"
  ## Link to uninstaller i menyn
  ;CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$OUTDIR\$1" "" "$INSTDIR\unicon.ico"
 
  ;Store Start Menu Folder in registry
  !insertmacro MUI_STARTMENU_WRITE_END
  !define UninstallKey "Software\Microsoft\Windows\CurrentVersion\Uninstall\${INSTDIR}\"
  WriteRegStr HKLM "${UninstallKey}" "DisplayName"     "${INSTDIR} ${VERSION} (64-bit)"
  WriteRegStr HKLM "${UninstallKey}" "DisplayVersion"  "${VERSION}"
  WriteRegStr HKLM "${UninstallKey}" "DisplayIcon"     "$INSTDIR\icon.ico"
  WriteRegStr HKLM "${UninstallKey}" "UninstallString" "$INSTDIR\uninstall.exe"
  WriteRegStr HKLM "${UninstallKey}" "Publisher"       " ${PUBLICER}"

 
!macroend
 
;--------------------------------
;Pages
   !define MUI_WELCOMEPAGE_TITLE "$(WELCOME)"
  !insertmacro MUI_PAGE_WELCOME
 
  !insertmacro MUI_PAGE_LICENSE "License\LICENSE"
  !insertmacro MUI_PAGE_LICENSE "License\LICENSE_7-Zip"

  
  ;!insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
 
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\$(^Name)"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
 
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
 
  !insertmacro MUI_PAGE_INSTFILES
 
  ## Desktop shortcut
  ;Create desktop shortcut before reboot
  ;!insertmacro MUI_FINISHPAGE_SHORTCUT
 
 !insertmacro MUI_PAGE_FINISH
 
  ;Uninstaller pages
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
 
;--------------------------------
;Language Include
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Italian"
  !insertmacro MUI_LANGUAGE "Swedish"

 
;--------------------------------
;Reserve Files
 
  ;If you are using solid compression this will make installer start faster
  !insertmacro MUI_RESERVEFILE_LANGDLL
  

 
;--------------------------------
;Language Strings
 
  !define UNINST_LOCALIZE
  ; In language files

  
 
;--------------------------------
;Installer Sections
 
Section "nsisFileList" Sec1
	SectionIn 1 RO	
 
  ;Set selected in "Full" and "Minimal" install types (see InstType) and 
  ;Make unavailable to change section state
  ;SectionIn 1 2 RO
 
  ;Set output path ($OUTDIR) and create it recursively if necessary

  ;Create an exclusion list (UnInst.nsh)
  ;!insertmacro UNINSTALLER_DATA_BEGIN
 

!include filelist_install.nsh

SetOutPath "$INSTDIR"

 
 
  ;Get section name
  SectionGetText ${Sec1} $0

  ;Write uninstaller and create shortcuts
  !insertmacro SHORTCUTS "$0" "nsisfilelist.exe" "unicon.ico"
  SectionEnd


;--------------------------------
;Uninstaller Section
 
Section "Uninstall"

!include filelist_uninstall.nsh
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
 
  ;Delete shortcuts
  Delete "$SMPROGRAMS\$StartMenuFolder\$(^Name).lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\$(uninstall).lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  RMDir /r "$SMPROGRAMS\$StartMenuFolder"
 
  ;Delete "$DESKTOP\$(^Name).lnk"
  ;Desktop shortcut
  ;Delete "$DESKTOP\${BASE_NAME}.lnk"
 
  ;Remove from control panel programs list
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\uninstall\nsisFileList"
 
  ;Remove Start Menu Folder, language and installation folder from registry
  ;DeleteRegKey /ifempty HKCU "Software\$(^Name)"
   DeleteRegKey HKCU "Software\$(^Name)"
SectionEnd
 
;--------------------------------
;Installer Functions
 
Function .onInit
 SetRegView 64
  Call UninstallPrevious
  ;Display language selection dialog
  !insertmacro MUI_LANGDLL_DISPLAY
 
FunctionEnd
 
Function .onGUIEnd
 
  ;Store installation folder in registry
  WriteRegStr HKCU "Software\$(^Name)" "" $INSTDIR
 
  ;Escape from $PLUGINSDIR to completely remove it
  SetOutPath "$INSTDIR"

FunctionEnd
 ;Desktop shortcut
;Function CreateDesktopShortCut
;SetShellVarContext current
 
  ;CreateShortCut "$DESKTOP\${BASE_NAME}.lnk" "$INSTDIR\${EXECUTABLE}" "" "$INSTDIR\icon.ico"
  ;CreateShortCut "$DESKTOP\$(^Name).lnk" "$INSTDIR\${EXECUTABLE}" "" "$INSTDIR\icon.ico"
 
;FunctionEnd
 
;--------------------------------
;Uninstaller Functions

Function un.onInit

 SetRegView 64

  ;Get stored language preference
  !insertmacro MUI_UNGETLANGUAGE
 
FunctionEnd
 
Function un.onUninstSuccess
 
  MessageBox MB_OK|MB_ICONINFORMATION "$(UNCOMPLATE)" /SD IDOK
 
FunctionEnd
