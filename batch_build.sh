#!/bin/bash

QT5=5.15.14
QT6=6.7.1
EXECUTABLE=checkversionsvtplaydl

THISPLACE=`pwd`
SOKVAG="${THISPLACE}/code"




		echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Qt5.15.2" "Qt${QT5}" "Qt${QT5}-32" "Qt${QT6}" "Qt6.4.1 64-bit static" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Qt5.15.2")
            AppDir="AppDirQt5"
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            build_executable="build-executable5"
            break;
            ;;
        "Qt${QT5}")
            AppDir="AppDirQt5"
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/${QT5}/gcc_64/bin/qmake"
            build_executable="build-executable5"
            export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/${QT5}/gcc_64/bin/:$PATH
            break;
            ;;
        "Qt${QT5}-32")
            AppDir="AppDirQt5"
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/${QT5}/gcc_32/bin/qmake"
            build_executable="build-executable5"
            export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_32/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/${QT5}/gcc_32/bin/:$PATH
            break;
            ;;
        "Qt${QT6}")
            AppDir="AppDirQt6"
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/${QT6}/gcc_64/bin/qmake"
            build_executable="build-executable6"
            export LD_LIBRARY_PATH=/opt/Qt/${QT6}/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/${QT6}/gcc_64/bin:$PATH
            break;
            ;;
        "Qt6.4.1 64-bit static")
            AppDir="AppDirQt6"
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/Qt6.4.1_static/bin/qmake"
            build_executable="build-executable6"
            export LD_LIBRARY_PATH=/opt/Qt/Qt6.4.1_static/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/Qt6.4.1_static/bin:$PATH
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Debug" "Release" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Debug")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ${SOKVAG}/*.pro
			$qmakePath ${SOKVAG}/*.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
			make -j$(nproc)
			make clean -j$(nproc)
			cd ..
			rm -r build
            break;
            ;;
        "Release")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ${SOKVAG}/*.pro
			if [ "$bit32" > 0 ] ;
			then 
				$qmakePath ${SOKVAG}/*.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
			else
				$qmakePath ${SOKVAG}/*.pro -spec linux-g++ CONFIG+=release CONFIG+=qml_release && /usr/bin/make qmake_all
			fi
			
			                          
			/usr/bin/make -j$(nproc)
			/usr/bin/make clean -j$(nproc)
			cd ..
			rm -r build
			#
						    echo -----------------------------------------------------------
							PS3='Please enter your choice: '
							options=("Copy to ${AppDir}" "Quit")
							select opt in "${options[@]}"
					do
						case $opt in
							"Copy to ${AppDir}")
								sokvag=`pwd`
								cp -f $build_executable/${EXECUTABLE} ${AppDir}/usr/bin/
								break;
								;;
							"Quit")
								break
								;;
							*) echo "invalid option $REPLY";;
						esac
					done
		#
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done



# AppImage
        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Run this script again" "Build AppImage" "Do not build AppImage" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
            "Run this script again")
            echo "You chose choice $REPLY which is $opt"
            ./`basename "$0"`

            break;
            ;;
        "Build AppImage")
            echo "You chose choice $REPLY which is $opt"
            ./build_appimage.sh
            break;
            ;;
        "Do not build AppImage")
            echo "You chose choice $REPLY which is $opt"
            echo "Goodbye"
	        exit 0
            break;
            ;;
        "Quit")
            break
            exit 0;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


